# -*- coding: utf-8 -*-
"""
Created on Thu Apr 20 18:44:36 2017

@author: media
"""
#すべてのフォルダに対してラベルを作成するソフト
import glob
import cv2
import numpy
def output_right_y(height, width, images):#右下のy座標を求める
        for h in reversed(range(height)): 
            for w in reversed(range(width)):
                if all([images[h, w][0] == 255, images[h, w][0] == 255, images[h, w][0] == 255]):
                   return h           

def output_right_x(height, width, images):#右下のx座標を求める
        for w in reversed(range(width)): 
            for h in reversed(range(height)):
                if all([images[h, w][0] == 255, images[h, w][0] == 255, images[h, w][0] == 255]):
                   return w
                   
def output_width_heigh(left_x, left_y, right_x, right_y):#物体領域のwidth, height を出力
    return [right_x - left_x, right_y - left_y]
#apple_1, apple_2, apple_3, apple_4, apple_5とフォルダがあった時 apple_input_binary_images, apple_input_xyを作成する

all_class = sorted(glob.glob('*')) #すべてのクラスを取得　（ここで配列番号がclass番号になる
all_class_num = len(all_class) - 1 #クラス数
print all_class
print 'クラス数: ' + str(all_class_num)
for k in range(50, all_class_num, 1): #クラス数分のループ
    
    #class_name = raw_input('クラス名を入力: ')            
    class_name = str(k)
    
    folder_allname = sorted(glob.glob(all_class[k] + '/*'))

    #folder_classname = folder_allname[0].split('_1')
    folder_name = all_class[k]
    
    print folder_name

    folders_num = len(folder_allname) #フォルダの個数
    print 'フォルダの個数:' + str(folders_num)


    for j in range(0, folders_num, 1): #フォルダの個数-1分のループ 
       
       mask_list = sorted(glob.glob(folder_allname[j] + '/*mask.png'))
       xy_list = sorted(glob.glob(folder_allname[j] + '/*loc.txt'))
       files = len(mask_list)
       for i in range(files):#フォルダ内のループ(apple_1のみ　など)
            print(str(i) + '枚目の入力ファイル:' + xy_list[i])
            print(str(i) + '枚目の入力画像ファイル:' + mask_list[i])
            f = open(xy_list[i], 'r') #xyファイルの読み込み
            images = cv2.imread(mask_list[i]) #画像ファイの読み込み
       
            xy_num = f.read().split(',')
            left_x = float(xy_num[0])
            left_y = float(xy_num[1])
        
            pic_height, pic_width, c = images.shape
        
            right_y = float(output_right_y(pic_height, pic_width, images))
            right_x = float(output_right_x(pic_height, pic_width, images))
        
            width, height = output_width_heigh(left_x, left_y, right_x, right_y)
        
            tmp1_label_name = mask_list[i].split('/')
            tmp2_label_name = tmp1_label_name[2].split('_mask')
            label_name = tmp2_label_name[0]
        
            write_file = open(folder_name + '/' + folder_name + '_' + str(j + 1) + '/' + label_name + '.txt', 'w')
            write_file.write(str(class_name) + ' ' + str(left_x/pic_width) + ' ' + str(left_y/pic_height) + ' ' + str(width/pic_width) + ' ' + str(height/pic_height))
            print('書き込みました:::' + str(class_name) + ' ' + str(left_x/pic_width) + ' ' + str(left_y/pic_height) + ' ' + str(width/pic_width) + ' ' + str(height/pic_height))

        
        
        
        
        
        
