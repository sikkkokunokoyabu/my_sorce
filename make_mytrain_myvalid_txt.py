# -*- coding: utf-8 -*-
"""
Created on Fri Apr 28 16:20:12 2017

@author: media
"""

#images_preに入っているファイルからmytrain_pre, myvalid_preに書き込むソフト
import random
import glob

all_file = sorted(glob.glob('images_pre/*'))
all_file_num = len(all_file)
train = open('mytrain_pre.txt', 'a')
valid = open('myvalid_pre.txt', 'a')

for i in range(all_file_num):
    rand = random.randint(1, 10)
    if rand == 1:   
        print 'validに書き込み'
        valid.write('/home/media/mydata_train/darknet/data/mytrain/' + all_file[i] + '\n')
    else:
        print 'trainに書き込み'
        train.write('/home/media/mydata_train/darknet/data/mytrain/' + all_file[i] + '\n')
