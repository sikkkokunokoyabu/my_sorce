# -*- coding: utf-8 -*-
"""
Created on Mon Apr 24 16:09:56 2017

@author: media
"""

import glob
import shutil

#imagesフォルダとlabelsフォルダをつくる
#改善1:imagesとlabelsのファイル数を合わせるためにlabelsからファイル名を取得

all_class = sorted(glob.glob('rgbd-dataset-full/*')) #フォルダ構成によって名前変更
all_class_num = len(all_class)
print 'クラス数: ' + str(all_class_num)

for k in range(all_class_num):
    folder_allname = sorted(glob.glob(all_class[k] + '/*'))
    folders_num = len(folder_allname)
    
    for j in range(0, folders_num, 1): #フォルダの個数-1分のループ 
        #image_list = sorted(glob.glob(folder_allname[j] + '/*[0-9].png'))
        label_list = sorted(glob.glob(folder_allname[j] + '/*[0-9].txt'))
        files = len(label_list)
        #print '画像枚数: ' + str(len(image_list))
        print 'ラベル枚数: ' + str(len(label_list))
        
    
        for i in range(files): #フォルダ内のファイル数分ループ
            now = label_list[i].split('.txt') #ラベルからimageの名前も決定する
            image_list_now = now[0] + '.png' 
            print(str(i) + '枚目のコピーラベル:' + label_list[i])
            print(str(i) + '枚目のコピー画像:' + image_list_now)
            tmp1_label_name = label_list[i].split('/')
            label_name = tmp1_label_name[3]
            tmp1_image_name = image_list_now.split('/')
            image_name = tmp1_image_name[3]
            try:
                shutil.copyfile(image_list_now, 'images/' + image_name)
                shutil.copyfile(label_list[i], 'labels/' + label_name)
            except:
                print(image_list_now + '::ファイルが存在しない')
        