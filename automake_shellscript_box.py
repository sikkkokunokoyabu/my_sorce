# -*- coding: utf-8 -*-
"""
Created on Mon Apr 17 14:58:20 2017

@author: media
"""

import glob
#wheight = raw_input('wheightを入力: ')
#thresh = raw_input('thresh(しきい値)を入力: ')
#file_num = raw_input('ファイル数を入力: ')
weight = ['10000', '25000', 'final'] 
thresh = ['0.1', '0.2', '0.3', '0.4', '0.5', '0.6', '0.7', '0.8', '0.9']
allfiles = sorted(glob.glob('data/mytrain/images_test/all/*.png'))
file_num = len(allfiles)
for k in range(len(weight)):
    for j in range(len(thresh)):
        f = open('box_weight' + weight[k] + '_thresh' + thresh[j] + '.sh', 'a')
        for i in range(file_num):
            f.write("./darknet detector box cfg/mytrain.dataset cfg/tiny-yolo-ppap.cfg tiny-yolo-ppap_"+ weight[k] +".weights " + allfiles[i] + " -thresh "+ thresh[j] + "\n")
        f.close()
        
