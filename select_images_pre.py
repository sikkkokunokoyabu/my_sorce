# -*- coding: utf-8 -*-
"""
Created on Fri Apr 28 14:18:03 2017

@author: media
"""

#pre_train用画像を乱数を用いて選別する
import glob
import random
import shutil

rate = raw_input('pre_trainに利用する画像枚数の割合を入力(1~10): ')
rate = int(rate)
all_file = sorted(glob.glob('images_pre/*'))
all_file_num = len(all_file)
files = int(all_file_num * (rate/10.0))
print(str(all_file_num) + '枚から' + str(files) +'枚選びます')
for i in range(files):
    rand = random.randint(0, all_file_num-1)
    print('選ばれしファイル:' + all_file[rand])
    
    name = all_file[rand].split('/')
    shutil.copyfile(all_file[rand], 'images_pre_selected/' + name[1])