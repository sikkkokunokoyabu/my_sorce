# -*- coding: utf-8 -*-
import glob
import cv2

def output_ratio(ans, pre_now):
    intersection_x = max(min(int(ans[1])+int(ans[3]), int(pre_now[1])+int(pre_now[3])) - max(int(ans[1]), int(pre_now[1])), 0) #ansとpreの共通部分のwidth
    intersection_y = max(min(int(ans[2])+int(ans[4]), int(pre_now[2])+int(pre_now[4])) - max(int(ans[2]), int(pre_now[2])), 0) #ansとpreの共通部分のheight
    area_intersection = intersection_x * intersection_y
    area_true = int(ans[3])*int(ans[4])
    area_pre = int(pre_now[3])*int(pre_now[4])        
    area_union = area_true + area_pre - area_intersection #ansとpre_nowの合計部分の面積
    ratio = area_intersection/float(area_union)
    return ratio
    

def output_ap(ans, pre, pre_num): #ansは正解のラベル1,
    print 'do_output_ap'
    sum_ratio = 0.0
    sikiiti = 0.7 #検出のみなすしきい値 : 変更可能
    ans_class = ans[0] #正解のクラス
    for j in range(pre_num): #予測されたbox分のループ
        pre_now = pre[j].split(' ') #予測されたboxから1つを取り出す
        pre_class = pre_now[0] #j番目の予測されたクラスの種類
        if(ans_class == pre_class): #クラスが一致した時のみ位置が一致するかの計算を行う
           if(float(output_ratio(ans, pre_now)) >= float(sikiiti)): #値がしきい値以上ならば検出されたということ
               sum_ratio += 1
    return sum_ratio/float(pre_num) #合計検出数/boxの数:1枚の画像の評価値
            
            

thresh = ['0.50', '0.60', '0.70', '0.80']
all_ans_file = sorted(glob.glob('ans/*'))
all_img_file = sorted(glob.glob('images/*'))
weight = raw_input('weightを入力: ')
all_file_num = len(all_ans_file)

for t in range(len(thresh)):
    all_prediction_file = sorted(glob.glob('result/*' + str(thresh[t]) + '.txt'))
    all_prediction_file_num = len(all_prediction_file)
    accuracy_i = 0.0 #評価値の初期化 : threshによって別で計算する予定    
    accuracy = 0.0 
    for i in range(all_prediction_file_num): #threshがtの画像枚数分ループ
        print '100正解ファイル:' + all_ans_file[i]
        print '正解画像:' + all_img_file[i]
        print '予測ファイル:' + all_prediction_file[i]
        
        img = cv2.imread(all_img_file[i])
        height, width, c = img.shape[:3]
        print '画像サイズ:' + str(width) + '*' + str(height)
        f_ans = open(all_ans_file[i], 'r')
        f_pre = open(all_prediction_file[i], 'r')
        ans = f_ans.read().split(' ') #正解がファイル 
        pre = f_pre.read().split('\n') #予測ファイル 検出されたbox数分の行がある
        pre_num = len(pre) - 1 #検出されたbox数
        
        #正解ラベルの大きさ合わせ
        ans[1] = float(ans[1])*width
        ans[2] = float(ans[2])*height
        ans[3] = float(ans[3])*width
        ans[4] = float(ans[4])*height
        print('cal_ap')
        
        accuracy_i += output_ap(ans, pre, pre_num) #i番目の画像の評価値を足していく
    accuracy = accuracy_i/all_prediction_file_num #threshがtのすべての画像の評価値を画像枚数で割る
    print('threshが' + str(t) + 'の画像郡のaccuracyは' + str(accuracy))
    g = open('accuracy_region', 'a')
    g.write('weight:' + weight + ', しきい値:' + str(thresh[t]) + ', accuracy:' + str(accuracy) + '\n') 
    g.close()
    f_ans.close()
    f_pre.close()
        
        
                
        
        
        
        
        
        
        
        
        
        
        
         