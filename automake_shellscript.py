# -*- coding: utf-8 -*-
"""
Created on Mon Apr 17 14:58:20 2017

@author: media
"""


#wheight = raw_input('wheightを入力: ')
#thresh = raw_input('thresh(しきい値)を入力: ')
#file_num = raw_input('ファイル数を入力: ')
weight = ['100', '200', '500', '1000', '10000', '25000', 'final'] 
thresh = ['0.1', '0.2', '0.3', '0.4', '0.5', '0.6', '0.7', '0.8', '0.9', '1.0']
file_num = 1032
for k in range(len(weight)):
    for j in range(len(thresh)):
        f = open('weight' + weight[k] + '_thresh' + thresh[j] + '.sh', 'a')
        for i in range(file_num):
            f.write("./darknet detector test cfg/ppap.dataset cfg/tiny-yolo-ppap.cfg tiny-yolo-ppap_"+ weight[k] +".weights '/home/media/YOLO_train2/darknet/data/ppap/images/"+ "{0:05d}".format(i) + ".png' -thresh "+ thresh[j] + "\n")
        f.close()
        