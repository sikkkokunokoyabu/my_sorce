# -*- coding: utf-8 -*-
"""
Created on Wed Apr 19 17:30:37 2017

@author: media
"""
import glob

i = 0
#labels_file = raw_input('ラベルフォルダ名を入力: ')


#thresh = raw_input('threshを入力: ')

allfiles = sorted(glob.glob('data/mytrain/images_test/all/*[0-9].png'))
thresh = ['0.50', '0.60', '0.70', '0.80']#しきい値
weight = raw_input('weightを入力: ')
files = len(allfiles)
for t in range(len(thresh)):
    prediction_file = 'weight' + weight + '/output_result' + thresh[t] + '.txt'

    #prediction_file = raw_input('予測ファイル名を入力: ')





    #cat_num = int(raw_input('クラス数を入力: ')) 

    f = open(prediction_file, 'r') #予測ファイル（1つのファイルにファイル数分の行がある）（例（0,2,1,4）（クラスが2個、クラス１が４個））
    n=0.0
    accurate = 0.0
    accuracy = 0.0
    
    for i in range(files):
        print str(i) + '枚目'
        tmp_label = allfiles[i].split('.png')
        now_label = tmp_label[0] + '.txt'
        g = open(now_label, 'r') #正解ファイル(ファイル数分ある)
        answer_label = g.read().split(" ")
        labels = int(answer_label[0])
        print('正解ラベルは' + str(labels))
        pre = f.readline().split(",") #予測ファイルの要素を順に詰めた配列 (pre[0] = 0, pre[1] = 2, pre[2] = 1, pre[3] = 4)
        
                                                                       #配列番号が奇数：検出数　　偶数:クラｽ
        print pre
        detection_sum = 0.0
        cat_num = len(pre) #予測ファイルの大きさを保存
        print '予測ファイルの大きさ' + str(cat_num) #i 0 1 2 3 
        for j in range(1, cat_num, 2): #検出された要素の合計を保存（例ではpre[1] + pre[3] = 6)
            preint = int(pre[j])
            detection_sum += preint
            
            
            #参考　ラベル ^ そのラベルの検出数が格納されている配列の番号 （ラベル*2 + 1）で求める （例0 ^ 1  1 ^ 3  2 ^ 5  3 ^ 7
        if detection_sum != 0:
            accurate += int(pre[labels * 2 + 1]) / detection_sum #正解ラベルの検出数を参照する
                
            print(str(i) + '枚目の正解数 = ' + str(pre[labels * 2 + 1]))
            print(str(i) + '枚目の検出された合計 = ' + str(detection_sum))
            print(str(i) + '枚目までのaccurate合計 = ' + str(accurate))
                
    accuracy = accurate/files
    print('最終accuracy = ' + str(accuracy))   
    g = open('output_accuracy2_test2', 'a')
    g.write('weight:' + weight + ', しきい値:' + str(thresh[t]) + ', accuracy:' + str(accuracy) + '\n') 
    f.close()
    g.close()
