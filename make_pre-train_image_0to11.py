# -*- coding: utf-8 -*-
"""
Created on Mon Apr 24 17:38:04 2017

@author: media
"""

#pre-train用画像生成

import glob
import cv2

def output_right_y(height, width, images):#右下のy座標を求める
        for h in reversed(range(height)): 
            for w in reversed(range(width)):
                if all([images[h, w][0] == 255, images[h, w][0] == 255, images[h, w][0] == 255]):
                   return h           

def output_right_x(height, width, images):#右下のx座標を求める
        for w in reversed(range(width)): 
            for h in reversed(range(height)):
                if all([images[h, w][0] == 255, images[h, w][0] == 255, images[h, w][0] == 255]):
                   return w




all_class = sorted(glob.glob('rgbd-dataset-full/*'))
all_class_num = len(all_class)
print 'クラス数: ' + str(all_class_num)

for k in range(0, 12, 1):
    folder_allname = sorted(glob.glob(all_class[k] + '/*'))
    folders_num = len(folder_allname)
    
    for j in range(0, folders_num, 1): #フォルダの個数-1分のループ 
        image_list = sorted(glob.glob(folder_allname[j] + '/*[0-9].png'))
        xy_list = sorted(glob.glob(folder_allname[j] + '/*loc.txt'))
        mask_list = sorted(glob.glob(folder_allname[j] + '/*mask.png'))
        files = len(image_list)
        print '画像枚数: ' + str(len(image_list))
        print 'ラベル枚数: ' + str(len(xy_list))
    
        for i in range(files): #フォルダ内のファイル数分ループ
            try:
                now = image_list[i].split('.png')
                now_xy = now[0] + '_loc.txt'
                now_mask = now[0] + '_mask.png'
                print(str(i) + '枚目のxyファイル:' + now_xy)
                print(str(i) + '枚目の画像ファイル:' + image_list[i])
                print(str(i) + '枚目のマスク画像:' + now_mask)
                f = open(now_xy, 'r') 
                images = cv2.imread(image_list[i])
                mask_images = cv2.imread(now_mask)
            
                xy_num = f.read().split(',')
                left_x = int(xy_num[0])
                left_y = int(xy_num[1])
            
                pic_height, pic_width, c = mask_images.shape
            
                right_y = int(output_right_y(pic_height, pic_width, mask_images))
                right_x = int(output_right_x(pic_height, pic_width, mask_images))
                
                out_image = images[left_y:right_y, left_x:right_x]
            
         
         #   tmp1_xy_name = xy_list[i].split('/')
         #   xy_name = tmp1_xy_name[3]
                tmp1_image_name = image_list[i].split('/')
                image_name = tmp1_image_name[3].split('.')
            
                cv2.imwrite('images_pre/' + image_name[0] + '_c' + str(k) + '.png', out_image)
            except:
                print (str(i) + "は存在しない")
           
